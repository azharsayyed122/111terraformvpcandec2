
VPC_CIDR             = "172.31.0.0/16"
PROJECTNAME          = "student"
ENV                  = "dev"
PUBLIC_SUBNET_CIDR =["172.31.0.0/24","172.31.1.0/24"] 
PRIVATE_SUBNET_CIDR =["172.31.2.0/24","172.31.3.0/24"]
DEFAULT_VPC_ID       = "vpc-0863b71319d9881fe"
BUCKET               = "azzyartifcat"
