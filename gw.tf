resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "${var.PROJECTNAME}-${var.ENV}-gw"

  }
}

// elastic IP address DO WITH CAUTION 

resource "aws_eip" "eip" {
  vpc      = true
}

resource "aws_nat_gateway" "ngw" {
  allocation_id = aws_eip.eip.id
  subnet_id     = aws_subnet.public.*.id[0]  // to add the subntets to as many timies its created 

  tags = {
    Name = "${var.PROJECTNAME}-${var.ENV}-ngw"
  }

// this ensures that this resrouce can only be cerated after ceration of EIP
  depends_on = [aws_eip.eip]
}