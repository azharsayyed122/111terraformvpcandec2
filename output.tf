output "VPC_ID" {
    value = aws_vpc.main.id
}


output "PUB_SUBNET" {
    value = aws_subnet.public.*.id
}

output "STUDENT_NETWORK" {
    value = var.VPC_CIDR
}
 