resource "aws_route_table" "public" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

# Peering Route ; Data is extracted from the dataSource
  route {
    cidr_block    = data.aws_vpc.default.cidr_block
    vpc_peering_connection_id = aws_vpc_peering_connection.peer.id
  }

  tags = {
    Name = "${var.PROJECTNAME}-${var.ENV}-public-rt"
  }
}


resource "aws_route_table" "private" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.ngw.id
  }

  tags = {
      Name = "${var.PROJECTNAME}-${var.ENV}-private-rt"
  }
}


# Associating the Public Route-Tables to the Public Subnets
resource "aws_route_table_association" "public" {
 
  count      =  length(var.PUBLIC_SUBNET_CIDR) 
  subnet_id      = element(aws_subnet.public.*.id, count.index)
  route_table_id = aws_route_table.public.id
}

# Associating the Private Route-Tables to the Private Subnets
resource "aws_route_table_association" "private" {
 
  count      =  length(var.PRIVATE_SUBNET_CIDR) 
  subnet_id      = element(aws_subnet.private.*.id, count.index)
  route_table_id = aws_route_table.private.id
}



# Adding Student VPC as Route to the Default VPC RouteTable 
resource "aws_route" "default-to-student" {
  route_table_id            = data.aws_route_table.def_vpc_routetable.route_table_id
  destination_cidr_block    = var.VPC_CIDR
  vpc_peering_connection_id = aws_vpc_peering_connection.peer.id
}
